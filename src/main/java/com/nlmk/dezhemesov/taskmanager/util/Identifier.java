package com.nlmk.dezhemesov.taskmanager.util;

/**
 * Псевдо-обёртка для хранения идентификатора
 */
public class Identifier {
    /**
     * Значение идентификатора
     */
    private Long identifier = null;

    /**
     * Конструетор по умолчанию
     */
    public Identifier() {
    }

    /**
     * Конструктор с инициирующим значением
     *
     * @param identifier значение идентификатора
     */
    public Identifier(final Long identifier) {
        this.identifier = identifier;
    }

    /**
     * Получение значения
     *
     * @return значение идентификатора
     */
    public Long getIdentifier() {
        return identifier;
    }

    /**
     * Установка значения
     *
     * @param identifier значение идентификатора
     */
    public void setIdentifier(final Long identifier) {
        this.identifier = identifier;
    }
}

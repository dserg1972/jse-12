package com.nlmk.dezhemesov.taskmanager.repository;

import com.nlmk.dezhemesov.taskmanager.entity.User;
import com.nlmk.dezhemesov.taskmanager.enumerated.Role;

import java.util.ArrayList;
import java.util.List;

/**
 * Репозиторий пользователей
 */
public class UserRepository {

    /**
     * Хранилище пользователей
     */
    private List<User> users = new ArrayList<>();

    /**
     * Создание пользователя и добавление в репозиторий
     *
     * @param login    логин
     * @param password пароль
     * @return созданный пользователь
     */
    public User create(final String login, final String password, final Role role) {
        User user = new User(login);
        users.add(user);
        user.setPassword(password);
        user.setRole(role);
        return user;
    }

    /**
     * Поиск пользователя в репозитории
     *
     * @param login логин
     * @return найденный пользователь
     */
    public User findByLogin(final String login) {
        return users.stream().filter(u -> u.getLogin().equals(login)).findFirst().orElse(null);
    }

    /**
     * Поиск пользователя по идентификатору
     *
     * @param id ИД пользователя
     * @return пользователь
     */
    public User findById(final Long id) {
        return users.stream().filter(u -> u.getId().equals(id)).findFirst().orElse(null);
    }

    /**
     * Получение списка всех пользователей
     *
     * @return список пользователей
     */
    public List<User> findAll() {
        return users;
    }

    /**
     * Удаление пользователя
     *
     * @param login имя учётной записи
     * @return пользователь либо null, если такого нет
     */
    public User removeByLogin(final String login) {
        User user = findByLogin(login);
        if (user == null) return null;
        users.remove(user);
        return user;
    }

}

package com.nlmk.dezhemesov.taskmanager.controller;

import com.nlmk.dezhemesov.taskmanager.entity.User;
import com.nlmk.dezhemesov.taskmanager.enumerated.Role;
import com.nlmk.dezhemesov.taskmanager.exception.DuplicateUserException;
import com.nlmk.dezhemesov.taskmanager.repository.UserRepository;
import com.nlmk.dezhemesov.taskmanager.service.UserService;
import com.nlmk.dezhemesov.taskmanager.util.Hash;
import com.nlmk.dezhemesov.taskmanager.util.Identifier;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * Контроллер пользователей
 */
public class UserController extends AbstractController {

    /**
     * Сервис пользователей
     */
    private UserService userService;

    /**
     * Текущий пользователь
     */
    private User currentUser = null;

    /**
     * Идентификатор текущего пользователя
     */
    private Identifier userId = null;

    /**
     * Конструктор
     *
     * @param userService сервис пользователей
     */
    public UserController(UserService userService, Identifier userId) {
        this.userService = userService;
        this.userId = userId;
    }

    /**
     * Процедура аутентификации пользователя
     *
     * @return код возврата
     */
    public int login() {
        System.out.println("[USER AUTHENTICATION]");
        System.out.print("Enter login: ");
        final String login = readString();
        if (login == null || "".equals(login)) {
            System.out.println("Login can't be empty!");
            return -1;
        }
        System.out.print("Enter password: ");
        String password = readPassword();
        User user = userService.findByLogin(login);
        if (user == null || !Hash.generateMD5(password).equals(user.getPasswordDigest())) {
            System.out.println("Invalid login or password!");
            return -1;
        }
        currentUser = user;
        userId.setIdentifier(user.getId());
        viewCurrent();
        return 0;
    }

    /**
     * Вывод текущего пользователя
     *
     * @return код возврата
     */
    public int viewCurrent() {
        System.out.println("[NOW CURRENT USER IS: \"" + currentUser.getLogin() + "\" with \"" + currentUser.getRole().name() + "\" ROLE]");
        return 0;
    }

    /**
     * Процедура создания нового пользователя
     *
     * @return код возврата
     */
    public int create() {
        if (!currentUser.getRole().isAdmin()) {
            System.out.println("[YOU HAVE NOT PRIVILEGES FOR THIS]");
            return 0;
        }
        System.out.println("[USER CREATING]");
        System.out.print("Enter login: ");
        final String login = readString();
        if (login == null || "".equals(login)) {
            System.out.println("Login can't be empty!");
            return 0;
        }
        User userFind = userService.findByLogin(login);
        if (userFind != null) {
            System.out.println("User already exists!");
            return 0;
        }
        System.out.print("Enter password: ");
        String password = readPassword();
        Role[] roles = Role.values();
        System.out.print("Enter role (" +
                Arrays.stream(roles).map(r -> r.name()).collect(Collectors.joining(", ")) + "): ");
        String roleName = readString();
        Role role = Arrays.stream(roles).filter(r -> r.name().equals(roleName)).findFirst().orElse(null);
        if (role == null) {
            System.out.println("Invalid role");
            return 0;
        }
        User user = null;
        try {
            user = userService.create(login, password, role);
        } catch (DuplicateUserException e) {
        }
        optional:
        {
            System.out.print("Enter Surname (or ENTER to break): ");
            String surname = readString();
            if (surname == null || surname.equals(""))
                break optional;
            user.setSurname(surname);
            System.out.print("Enter Name (or ENTER to break): ");
            String name = readString();
            if (name == null || name.equals(""))
                break optional;
            user.setName(name);
            System.out.print("Enter Second Name (or ENTER to break): ");
            String secondName = readString();
            if (secondName == null || secondName.equals(""))
                break optional;
            user.setSecondName(secondName);
        }
        System.out.println("[USER CREATED]");
        return 0;
    }

    /**
     * Процедура вывода списка пользователей
     *
     * @return код возврата
     */
    public int list() {
        System.out.println("[LIST USERS]");
        int i = 1;
        for (User user : userService.findAll())
            System.out.println("[" + (i++) + "]: " + user);
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Процедура корректировки данных пользователя
     *
     * @return код возврата
     */
    public int update() {
        if (!currentUser.getRole().isAdmin()) {
            System.out.println("[YOU HAVE NOT PRIVILEGES FOR THIS]");
            return 0;
        }
        System.out.println("[CHANGE USER DATA]");
        System.out.print("Enter login: ");
        final String login = readString();
        if (login == null || login.isEmpty()) {
            System.out.println("Login can't be empty!");
            return 0;
        }
        final User user = userService.findByLogin(login);
        if (user == null) {
            System.out.println("User not found!");
            return 0;
        }
        System.out.print("Enter new password (or ENTER to leave current): ");
        final String password = readPassword();
        if (password != null && !password.equals(""))
            user.setPassword(password);
        Role[] roles = Role.values();
        System.out.print("Enter role (" +
                Arrays.stream(roles).map(r -> r.name()).collect(Collectors.joining(", ")) +
                ") [" + user.getRole().name() + "]: ");
        final String roleName = readString();
        Arrays.stream(roles).filter(r -> r.name().equals(roleName)).findFirst().ifPresent(user::setRole);
        System.out.print("Enter new surname [" + user.getSurname() + "]: ");
        final String surname = readString();
        if (surname != null && !surname.equals(""))
            user.setSurname(surname);
        System.out.print("Enter new name [" + user.getName() + "]: ");
        final String name = readString();
        if (name != null && !name.equals(""))
            user.setName(name);
        System.out.print("Enter new secondname [" + user.getSecondName() + "]: ");
        final String secondname = readString();
        if (secondname != null && !secondname.equals(""))
            user.setSecondName(secondname);
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Удаление пользователя
     *
     * @return код возврата
     */
    public int remove() {
        if (!currentUser.getRole().isAdmin()) {
            System.out.println("[YOU HAVE NOT PRIVILEGES FOR THIS]");
            return 0;
        }
        System.out.println("[CHANGE USER DATA]");
        System.out.print("Enter login: ");
        final String login = readString();
        if (login == null || login.isEmpty()) {
            System.out.println("[USER NOT EXISTS]");
            return 0;
        }
        if (login.equals(currentUser.getLogin())) {
            System.out.println("[CANNOT REMOVE HIMSELF]");
            return 0;
        }
        userService.removeByLogin(login);
        System.out.println("[USER " + login + " [REMOVED]");
        return 0;
    }

}
